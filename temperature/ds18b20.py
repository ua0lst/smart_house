import serial
from digitemp.master import UART_Adapter
from digitemp.device import DS18B20


def fixed_float(num_obj, digits=0):
    """The function to return fix digits"""
    return f"{num_obj:.{digits}f}"


def get_temp(temp_sensor: str) -> float:
    """The function to return value of temperature"""
    bus = UART_Adapter('/dev/ttyUSB0')  # DS9097 connected to COM1

    # only one 1-wire device on the bus:
    # sensor = DS18B20(bus)

    # specify device's ROM code if more than one 1-wire device on the bus:
    # sensor = DS18B20(bus, rom='28EE2BEB24160281')
    sensor = DS18B20(bus, temp_sensor)
    # display sensor's information
    # sensor.info()

    # get temperature
    temp_result = fixed_float(sensor.get_temperature(), 2)
    return float(temp_result)
