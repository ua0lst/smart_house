from DBcm import UseDatabase
from ds18b20 import get_temp
from time import sleep
from db_config import read_db_config

db_config = read_db_config()

sensors = dict()
sensors = {"Guest Hall": "28EE2BEB24160281", "Bathroom": "28EE2BEB24160281"}


while True:

    for key_sensor, value_sensor in sensors.items():
        temperature = get_temp(value_sensor)
        with UseDatabase(db_config) as cursor:
            _SQL = """insert into temperature
                   (temperature, locate_sensor)
                   values
                   (%s, %s)"""
            cursor.execute(_SQL, (temperature, key_sensor))
    sleep(300)
